//
//  TextGenerator.m
//  test
//
//  Created by Avvakumov Dmitry on 25.01.16.
//  Copyright © 2016 Dima Avvakumov. All rights reserved.
//

#import "TextGenerator.h"

@implementation TextGenerator

+ (NSString *) newReferat {
    static NSArray *dictionary = nil;
    if (dictionary == nil) {
        NSString *text = @"Винил, как бы это ни казалось парадоксальным, синхронно просветляет флэнжер, как и реверансы в сторону ранних \"роллингов\". Флэнжер, так или иначе, полифигурно выстраивает флюгель-горн. Форма представляет собой форшлаг. Легато дает кризис жанра. Кризис жанра имеет контрапункт контрастных фактур. Протяженность, так или иначе, выстраивает изоритмический рок-н-ролл 50-х. Пауза варьирует шоу-бизнес. Арпеджио варьирует лайн-ап. Хамбакер, в первом приближении, многопланово варьирует определенный хорус. Цикл продолжает паузный октавер. Форма иллюстрирует длительностный эффект \"вау-вау\", что отчасти объясняет такое количество кавер-версий. Плавно-мобильное голосовое поле, как бы это ни казалось парадоксальным, продолжает пласт, и здесь в качестве модуса конструктивных элементов используется ряд каких-либо единых длительностей. Арпеджио просветляет ритмоформульный райдер";
        
        dictionary = [text componentsSeparatedByString:@" "];
    }
    
    NSInteger max = [dictionary count];
    NSInteger count = arc4random_uniform((u_int32_t)max - 1) + 1;
    
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count; i++) {
        NSInteger index = arc4random_uniform((u_int32_t)max);
        
        [items addObject:[dictionary objectAtIndex:index]];
    }
    
    return [items componentsJoinedByString:@" "];
}

@end
