//
//  Message+CoreDataProperties.h
//  test
//
//  Created by Avvakumov Dmitry on 25.01.16.
//  Copyright © 2016 Dima Avvakumov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Message.h"

NS_ASSUME_NONNULL_BEGIN

@interface Message (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *text;
@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) NSString *dayOfDate;
@property (nullable, nonatomic, retain) NSString *timeOfDate;

@end

NS_ASSUME_NONNULL_END
