//
//  User+CoreDataProperties.m
//  test
//
//  Created by Avvakumov Dmitry on 04.02.16.
//  Copyright © 2016 Dima Avvakumov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User+CoreDataProperties.h"

@implementation User (CoreDataProperties)

@dynamic name;
@dynamic date;
@dynamic userID;

@end
