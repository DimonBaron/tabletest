//
//  Message+CoreDataProperties.m
//  test
//
//  Created by Avvakumov Dmitry on 25.01.16.
//  Copyright © 2016 Dima Avvakumov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Message+CoreDataProperties.h"

@implementation Message (CoreDataProperties)

@dynamic text;
@dynamic date;
@dynamic dayOfDate;
@dynamic timeOfDate;

@end
