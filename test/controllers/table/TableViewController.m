//
//  TableViewController.m
//  test
//
//  Created by Avvakumov Dmitry on 26.01.16.
//  Copyright © 2016 Dima Avvakumov. All rights reserved.
//

#import "TableViewController.h"

#import "SimpleTableViewController.h"

@interface TableViewController ()

@property (weak, nonatomic) IBOutlet UIButton *downButton;

@property (strong, nonatomic) SimpleTableViewController *tableController;

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"Table"]) {
        SimpleTableViewController *controller = (SimpleTableViewController *)segue.destinationViewController;
        
        self.tableController = controller;
    }
    
}

- (IBAction)downAction:(id)sender {
    [self.tableController scrollToBottom];
}

- (IBAction)addAction:(id)sender {
    [self.tableController addItem];
}

@end
