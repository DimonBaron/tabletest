//
//  SimpleTableViewController.m
//  test
//
//  Created by Avvakumov Dmitry on 25.01.16.
//  Copyright © 2016 Dima Avvakumov. All rights reserved.
//

#import "SimpleTableViewController.h"

// frameworks
#import <MagicalRecord/MagicalRecord.h>

#import "Message.h"
#import "SimpleCell.h"

#import "TextGenerator.h"

@interface SimpleTableViewController()

@property (assign, nonatomic) NSInteger countItems;
@property (assign, nonatomic) NSInteger fetchOffset;
@property (assign, nonatomic) NSInteger fetchLimit;

@end

@implementation SimpleTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.countItems = [Message MR_countOfEntities];
    self.fetchLimit = 20;
    self.fetchOffset = MAX(self.countItems - self.fetchLimit, 0) ;
    
    self.indexPathController = [[TLIndexPathController alloc] initWithFetchRequest:[self fetchRequest] managedObjectContext:[NSManagedObjectContext MR_defaultContext] sectionNameKeyPath:@"dayOfDate" identifierKeyPath:nil cacheName:nil];
//    [self refetchData];
    [self.indexPathController performFetch:nil];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    UINavigationController *navigationController = self.parentViewController.navigationController;
    CGFloat h = navigationController.navigationBar.frame.size.height + navigationController.navigationBar.frame.origin.y;
    [self.tableView setContentInset:UIEdgeInsetsMake(h, 0.0, 0.0, 0.0)];
    [self.tableView setScrollIndicatorInsets:UIEdgeInsetsMake(h, 0.0, 0.0, 0.0)];
    
    [self scrollTableViewToBottom:NO];
}

- (void)scrollTableViewToBottom:(BOOL)animated {
    CGFloat contentHeight = self.tableView.contentSize.height;
    CGFloat tableHeight = self.tableView.bounds.size.height;
    CGFloat tableInsetTop = 0.0; // self.tableView.contentInset.top + self.tableView.contentInset.bottom;
    CGFloat offsetY = contentHeight - tableHeight + tableInsetTop;
    [self.tableView setContentOffset:CGPointMake(0.0, offsetY) animated:animated];
}

- (BOOL)tableViewInBottomPosition {
    CGFloat contentHeight = self.tableView.contentSize.height;
    CGFloat tableHeight = self.tableView.bounds.size.height;
    CGFloat tableInsetTop = 0.0; // self.tableView.contentInset.top + self.tableView.contentInset.bottom;
    CGFloat offset = self.tableView.contentOffset.y;
    CGFloat bottomOffset = contentHeight - tableHeight + tableInsetTop;
    CGFloat diff = fabsf(bottomOffset - offset);
    
    if (diff < 3.0) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - Fetch

- (NSFetchRequest *)fetchRequest {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Message class])];
    NSSortDescriptor *dateSort = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
    request.sortDescriptors = @[dateSort];
    request.fetchLimit = self.fetchLimit;
    request.fetchOffset = self.fetchOffset;
    return request;
}

//- (void)refetchData {
//    // Message *message = [Message MR_];
//    
//    self.indexPathController.ignoreDataModelChanges = YES;
//    self.indexPathController.inMemoryPredicate = [NSPredicate predicateWithFormat:@"date > %@", date];
//    [self.tableView reloadData];
//    self.indexPathController.ignoreDataModelChanges = NO;
//}

#pragma mark - Public

- (void)scrollToBottom {
    [self scrollTableViewToBottom:YES];
}

#pragma mark - Table

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    Message *message = [self.indexPathController.dataModel itemAtIndexPath:indexPath];
    NSString *text = message.text;
    
    CGFloat maxWidth = [UIScreen mainScreen].bounds.size.width - 16.0;
    NSDictionary *attrs = @{ NSFontAttributeName: [UIFont systemFontOfSize:15.0f] };
    CGRect frame = [text boundingRectWithSize:CGSizeMake(maxWidth, 0.0) options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil];
    
    CGFloat height = frame.size.height;
    
    return 4.0 + height + 4.0 + 20.0;
}

- (void)tableView:(UITableView *)tableView configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Message *message = [self.indexPathController.dataModel itemAtIndexPath:indexPath];
    SimpleCell *callCell = (SimpleCell *)cell;
    
    callCell.titleLabel.text = message.text;
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    dateFormatter2.locale = [NSLocale systemLocale];
    dateFormatter2.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    dateFormatter2.dateFormat = @"HH:mm";
    NSString *timeOfDate = [dateFormatter2 stringFromDate:message.date];
    
    callCell.timeLabel.text = timeOfDate;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat offset = scrollView.contentOffset.y;

    if (offset < 100.0) {
        [self appendData];
    }
    
}

- (void)controller:(TLIndexPathController *)controller didUpdateDataModel:(TLIndexPathUpdates *)updates {
    // [self.tableView reloadData];
    [super controller:controller didUpdateDataModel:updates];
    
    if ([self tableViewInBottomPosition]) {
        // weak self
        __weak typeof (self) weakSelf = self;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            typeof (weakSelf) strongSelf = weakSelf;
            if (!strongSelf) {
                return;
            }
            
            [strongSelf scrollTableViewToBottom:YES];
        });
    }
    
}

- (void)appendData {
    static NSDate *lastDate = nil;
    NSDate *date = [NSDate date];
    
    if (lastDate && [date timeIntervalSinceDate:lastDate] < 1.0) {
        return;
    }
    lastDate = [NSDate date];
    
    self.fetchLimit += 20;
    self.fetchOffset = MAX(self.countItems - self.fetchLimit, 0) ;
    
    self.indexPathController.fetchRequest.fetchLimit = self.fetchLimit;
    self.indexPathController.fetchRequest.fetchOffset = self.fetchOffset;
    
    CGFloat oldContentOffsetY = self.tableView.contentOffset.y;
    CGFloat oldItemOffset = [self.tableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]].origin.y;
    Message *message = [self.indexPathController.dataModel itemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    self.indexPathController.ignoreDataModelChanges = YES;
    // self.rowAnimationStyle = UITableViewRowAnimationNone;
    [self.indexPathController performFetch:nil];
    // self.rowAnimationStyle = UITableViewRowAnimationAutomatic;
    self.indexPathController.ignoreDataModelChanges = NO;
    
    [self.tableView reloadData];
    
    NSIndexPath *newPath = [self.indexPathController.dataModel indexPathForItem:message];
    CGFloat offset = [self.tableView rectForRowAtIndexPath:newPath].origin.y;
    
    CGFloat newOffsetY = oldContentOffsetY + (offset - oldItemOffset);
    self.tableView.contentOffset = CGPointMake(0.0, newOffsetY);
}

- (IBAction)removeAction:(id)sender {
    
}

- (void)addItem {
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
        
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.locale = [NSLocale systemLocale];
        dateFormatter.timeZone = [NSTimeZone systemTimeZone];
        dateFormatter.dateFormat = @"yyyy-MM-dd";
        NSString *dayOfDate = [dateFormatter stringFromDate:date];
        
        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        dateFormatter2.locale = [NSLocale systemLocale];
        dateFormatter2.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        dateFormatter2.dateFormat = @"HH:mm";
        NSString *timeOfDate = [dateFormatter2 stringFromDate:date];
        
        Message *message = [Message MR_createEntityInContext:localContext];
        message.date = date;
        message.text = [TextGenerator newReferat];
        message.dayOfDate = dayOfDate;
        message.timeOfDate = timeOfDate;
        
    }];
}

@end
