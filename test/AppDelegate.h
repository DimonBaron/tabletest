//
//  AppDelegate.h
//  test
//
//  Created by Avvakumov Dmitry on 25.01.16.
//  Copyright © 2016 Dima Avvakumov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

