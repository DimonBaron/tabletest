//
//  ViewController.m
//  test
//
//  Created by Avvakumov Dmitry on 25.01.16.
//  Copyright © 2016 Dima Avvakumov. All rights reserved.
//

#import "ViewController.h"

// frameworks
#import <MagicalRecord/MagicalRecord.h>

#import "TextGenerator.h"
#import "Message.h"

@interface ViewController ()

@property (assign, nonatomic) BOOL isStarted;
@property (assign, nonatomic) NSInteger countAdded;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isStarted = NO;
    
    NSInteger count = [Message MR_countOfEntities];
    NSLog(@"Created: %d", count);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addNewPeark:(id)sender {
    [self initBase];
}

- (void)initBase {
    if (self.isStarted) return;
    self.countAdded = 0;
    self.isStarted = YES;
    
    [self appendPack];
}

- (void)appendPack {
    __block NSInteger countMessages = 10000;
    NSInteger onpage = 10;

    if (self.countAdded >= countMessages) {
        self.isStarted = NO;
        
        return;
    }
    
    // weak self
    __weak typeof (self) weakSelf = self;
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
        typeof (weakSelf) strongSelf = weakSelf;
        if (!strongSelf) {
            return;
        }
        
        for (int i = 0; i < onpage; i++) {
            
            u_int32_t days = (u_int32_t) countMessages / 10;
            u_int32_t secondInDay = 86400; // 60 * 60 * 24;
            NSTimeInterval time = arc4random_uniform(days * secondInDay);
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow: - time];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.locale = [NSLocale systemLocale];
            dateFormatter.timeZone = [NSTimeZone systemTimeZone];
            dateFormatter.dateFormat = @"yyyy-MM-dd";
            NSString *dayOfDate = [dateFormatter stringFromDate:date];
            
            NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
            dateFormatter2.locale = [NSLocale systemLocale];
            dateFormatter2.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
            dateFormatter2.dateFormat = @"HH:mm";
            NSString *timeOfDate = [dateFormatter2 stringFromDate:date];
            
            Message *message = [Message MR_createEntityInContext:localContext];
            message.date = date;
            message.text = [TextGenerator newReferat];
            message.dayOfDate = dayOfDate;
            message.timeOfDate = timeOfDate;
            
            strongSelf.countAdded++;
            if (self.countAdded >= countMessages) {
                break;
            }
            
            if (strongSelf.countAdded%100 == 0) {
                NSLog(@"iteration: %d", strongSelf.countAdded);
            }
            
        }
        
    } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
        typeof (weakSelf) strongSelf = weakSelf;
        if (!strongSelf) {
            return;
        }
        
        [strongSelf appendPack];
    }];
    
}

@end
