//
//  main.m
//  test
//
//  Created by Avvakumov Dmitry on 25.01.16.
//  Copyright © 2016 Dima Avvakumov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
